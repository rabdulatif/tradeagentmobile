﻿using Microsoft.EntityFrameworkCore;
using TradeAgent.Domain.Entities;

namespace TradeAgent.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class TradeAgentContext : DbContext
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual DbSet<User> Users { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual DbSet<TradePoint> TradePoints { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual DbSet<Product> Products { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public virtual DbSet<Order> Orders { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="options"></param>
        public TradeAgentContext(DbContextOptions<TradeAgentContext> options) : base(options) { }
    }
}
