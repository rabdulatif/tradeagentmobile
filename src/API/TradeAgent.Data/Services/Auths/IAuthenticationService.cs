﻿using TradeAgent.Domain.ApiModels;

namespace TradeAgent.Data.Services.Auths
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAuthenticationService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        UserApiModel Authenticate(string username, string password);
    }
}
