﻿using AutoMapper;
using TradeAgent.Domain.ApiModels;
using TradeAgent.Domain.Repositories;

namespace TradeAgent.Data.Services.Auths
{
    /// <summary>
    /// 
    /// </summary>
    public class AuthenticationService : IAuthenticationService
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IAuthenticationRepository _authenticationRepository;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="authenticationRepository"></param>
        /// <param name="mapper"></param>
        public AuthenticationService(IAuthenticationRepository authenticationRepository, IMapper mapper = null)
        {
            _authenticationRepository = authenticationRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public UserApiModel Authenticate(string username, string password)
        {
            var user = _authenticationRepository.Authenticate(username, password);
            if (user == null)
                return null;

            var userApiModel = _mapper.Map<UserApiModel>(user);
            userApiModel.Token = "Token";
            return userApiModel;
        }
    }
}
