﻿namespace TradeAgent.Data.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseRepository
    {
        /// <summary>
        /// 
        /// </summary>
        public TradeAgentContext Context { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public BaseRepository(TradeAgentContext context)
        {
            Context = context;
        }
    }
}
