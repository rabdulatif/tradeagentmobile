﻿using System.Linq;
using TradeAgent.Domain.Entities;
using TradeAgent.Domain.Repositories;

namespace TradeAgent.Data.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public class AuthenticationRepository : BaseRepository, IAuthenticationRepository
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IUserRepository Repository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public AuthenticationRepository(TradeAgentContext context, IUserRepository repository) : base(context)
        {
            Repository = repository;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public User Authenticate(string username, string password)
        {
            var user = Context.Users.FirstOrDefault(f => f.UserName == username && f.Password == password);
            return user;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        public void Register(User user)
        {
            Repository.Add(user);
        }
    }
}
