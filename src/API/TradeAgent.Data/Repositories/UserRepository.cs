﻿using System;
using System.Collections.Generic;
using System.Linq;
using TradeAgent.Domain.Entities;
using TradeAgent.Domain.Repositories;

namespace TradeAgent.Data.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public class UserRepository : BaseRepository, IUserRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public UserRepository(TradeAgentContext context) : base(context)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<User> GetAll()
        {
            return Context.Users.Where(w => !w.IsDeleted).OrderBy(o => o.CreatedOn).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User GetById(long id)
        {
            return Context.Users.FirstOrDefault(f => f.Id == id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public User Add(User user)
        {
            user.CreatedOn = DateTime.Now;
            Context.Users.Add(user);
            Context.SaveChanges();

            return user;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool Update(User user)
        {
            var item = GetById(user.Id);
            if (item == null)
                return false;

            user.ModifiedOn = DateTime.Now;
            Context.Entry(item).CurrentValues.SetValues(user);
            Context.SaveChanges();
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(long id)
        {
            var item = GetById(id);
            item.IsDeleted = true;
            item.ModifiedOn = DateTime.Now;

            Context.Entry(item).CurrentValues.SetValues(item);
            Context.SaveChanges();

            return true;
        }

    }
}
