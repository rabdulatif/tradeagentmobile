﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TradeAgent.Domain.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class Order : BaseEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public Order()
        {
            Products = new List<Product>();
        }

        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long TradePointId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public TradePoint TradePoint { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<Product> Products { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal TotalSales { get; set; }
    }
}
