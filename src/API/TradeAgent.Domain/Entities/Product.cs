﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TradeAgent.Domain.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class Product : BaseEntity
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ProductFactory { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Ostatka
        /// </summary>
        public decimal Rest { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime ExpirationDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int OrderQuantity { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Notes { get; set; }
    }
}
