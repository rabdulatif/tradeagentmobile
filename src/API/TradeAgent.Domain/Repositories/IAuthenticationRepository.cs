﻿using TradeAgent.Domain.Entities;

namespace TradeAgent.Domain.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAuthenticationRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        User Authenticate(string username, string password);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        void Register(User user);
    }
}
