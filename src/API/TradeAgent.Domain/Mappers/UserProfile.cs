﻿using AutoMapper;
using TradeAgent.Domain.ApiModels;
using TradeAgent.Domain.Entities;

namespace TradeAgent.Domain.Mappers
{
    /// <summary>
    /// 
    /// </summary>
    public class UserProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public UserProfile()
        {
            CreateMap<User, UserApiModel>();
            CreateMap<UserApiModel, User>();
        }
    }
}
