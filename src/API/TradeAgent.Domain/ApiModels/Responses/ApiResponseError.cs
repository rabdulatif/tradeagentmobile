﻿namespace TradeAgent.Domain.ApiModels
{
    /// <summary>
    /// 
    /// </summary>
    public class ApiResponseError
    {
        /// <summary>
        /// 
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ApiResponseError()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
        public ApiResponseError(int code, string message)
        {
            ErrorCode = code;
            ErrorMessage = message;
        }

        /// <summary>
        /// 
        /// </summary>
        public ApiResponseError(string message)
        {
            ErrorMessage = message;
        }
    }
}
