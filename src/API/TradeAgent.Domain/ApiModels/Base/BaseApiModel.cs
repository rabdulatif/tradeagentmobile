﻿using System;

namespace TradeAgent.Domain.ApiModels
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseApiModel
    {
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? ModifiedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long RowVersionValue { get; set; }
    }
}
