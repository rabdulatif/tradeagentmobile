﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TradeAgent.Data.Services.Auths;
using TradeAgent.Domain.ApiModels;

namespace TradeAgent.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IAuthenticationService Service;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        public AuthenticationController(IAuthenticationService service)
        {
            Service = service;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public ApiResponse<string> Authenticate(string username,string password)
        {
            var user = Service.Authenticate(username, password);
            if (user == null)
                return new ApiResponseError(400,"UserName or Password incorrect");

            return user.Token;
        }
    }
}
