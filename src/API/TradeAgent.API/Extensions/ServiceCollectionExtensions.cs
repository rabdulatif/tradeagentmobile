﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using TradeAgent.Data;
using TradeAgent.Data.Repositories;
using TradeAgent.Data.Services.Auths;
using TradeAgent.Data.Services.Users;
using TradeAgent.Domain.Mappers;
using TradeAgent.Domain.Repositories;

namespace TradeAgent.API.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public static void AddApiSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Trade Agent API",
                    Contact = new OpenApiContact
                    {
                        Name = "Abdulatif Rasulov",
                        Email = "latifrasulov10@gmail.com",
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Use under MIT",
                        Url = new Uri("https://opensource.org/licenses/MIT"),
                    }
                });
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        /// <param name="Configuration"></param>
        public static void AddSqlDbContext(this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddDbContext<TradeAgentContext>(
                options => options.UseSqlServer(
                Configuration["ConnectionStrings:ApplicationConnection"]));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public static void AddMapperProfiles(this IServiceCollection services)
        {
            var mapperConfig = new MapperConfiguration(config =>
            {
                config.AddProfile(new UserProfile());
            });

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public static void AddServices(this IServiceCollection services)
        {
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IUserService, UserService>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IAuthenticationRepository, AuthenticationRepository>();
        }
    }
}
