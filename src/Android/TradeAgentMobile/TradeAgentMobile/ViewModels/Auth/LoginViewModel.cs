﻿using TradeAgentMobile.Models.Auth;
using TradeAgentMobile.Views.Auth;
using Xamarin.Forms;

namespace TradeAgentMobile.ViewModels.Auth
{
    /// <summary>
    /// 
    /// </summary>
    public class LoginViewModel : BaseViewModel
    {
        /// <summary>
        /// 
        /// </summary>
        private LoginModel loginModel;

        #region Commands

        /// <summary>
        /// 
        /// </summary>
        public Command SignInCommand { get; }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string UserName
        {
            get { return loginModel.UserName; }
            set
            {
                if (loginModel.UserName == value) return;

                loginModel.UserName = value;
                SignInCommand.ChangeCanExecute();
                //NotifyPropertyChanged(nameof(UserName));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Password
        {
            get { return loginModel.Password; }
            set
            {
                if (loginModel.Password == value) return;

                loginModel.Password = value;
                SignInCommand.ChangeCanExecute();
                //NotifyPropertyChanged(nameof(Password));
            }
        }

        #endregion

        #region ctor

        /// <summary>
        /// 
        /// </summary>
        public LoginViewModel()
        {
            loginModel = new LoginModel();
            SignInCommand = new Command(Login, IsValid);
        }

        #endregion

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsValid() => !string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Password);

        /// <summary>
        /// 
        /// </summary>
        public async void Login()
        {
            Navigation?.InsertPageBefore(new MainTabbed(),LoginPage.Current);
            await Navigation?.PopToRootAsync();
        }

        #endregion
    }
}
