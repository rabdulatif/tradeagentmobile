﻿using System;
using TradeAgentMobile.Views.Auth;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TradeAgentMobile
{
    public partial class App : Application
    {
        /// <summary>
        /// 
        /// </summary>
        public static bool IsUserLoggedIn { get; set; }

        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new LoginPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
