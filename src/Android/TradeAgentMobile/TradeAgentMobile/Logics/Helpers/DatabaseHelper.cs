﻿using System.IO;

namespace TradeAgentMobile.Logics.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public class DatabaseHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetDatabasePath()
        {
            var storagePath = Path.Combine(Android.OS.Environment.ExternalStorageDirectory.Path, "PosDatabase");
            if (!Directory.Exists(storagePath))
                Directory.CreateDirectory(storagePath);

            string databasePath = Path.Combine(storagePath, "Pos.db");
            return databasePath;
        }
    }
}
