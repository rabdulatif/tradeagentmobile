﻿namespace TradeAgentMobile.Database.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class ApplicationUser : BaseEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Password { get; set; }
    }
}
